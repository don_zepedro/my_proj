<?php


namespace app\commands;
use GuzzleHttp\Client;
use yii\console\Controller;
use Yii;
use yii\console\ExitCode;
use phpQuery;
use app\models\Records;

class MyParcerController extends Controller
{
    public function actionParce()
    {
        $model = new Records();
        $client = new Client(['base_uri' => 'https://www.kinopoisk.ru']);
        $response = $client->get('/media/news/4004367/');
        $content = $response->getBody()->getContents();
        $phpquery =phpQuery::newDocument($content);
        $title = $phpquery->find('title')->html();
        $description = preg_replace('/\s?<a[^>]*?>.*?<\/a>\s?/si','',$phpquery->find('p.stk-reset')->html());
        $description = preg_replace('/\s?<em[^>]*?>.*?<\/em>\s?/si','',$description);
        $model->title = $title;
        $model->description = $description;
        try{
            $model->save();
        }catch (\Exception $e){
            print_r(array([
                'Error code' => $e->getCode(),
                'Error message' => $e->getMessage(),
            ])
            );
        }

        $email = Yii::$app->mailer->compose();
        $email->setTo('Anzhuyskiy@rambler.ru');
        $email->setSubject('Record add');
        $email->setTextBody('Record add by schedule');
        $email->send();
//        print_r($title . "\n\r" . $description . "\n\r");

    }

}