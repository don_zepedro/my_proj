<?php

namespace app\controllers;
use Yii;
use app\models\Records;

class Record1Controller extends \yii\web\Controller
{
    public function actionCreate()
    {
        $model = new Records();
        if($model->load(Yii::$app->request->post()) && $model->save()){
            Yii::$app->session->setFlash('success','Record added');
            return $this->redirect(['index']);
        }
        return $this->render('create',['model'=>$model]);
    }

    public function actionDelete($id)
    {
        $Record= Records::findOne($id);
        $Record->delete();
        Yii::$app->session->setFlash('success','Record has been deleted');
        return $this->redirect(['index']);

    }

    public function actionIndex()
    {
        $Records = Records::find()->asArray()->all();

        return $this->render('index',['Records'=>$Records]);
    }

    public function actionUpdate($id)
    {


        $Record= Records::findOne($id);

        if($Record->load(Yii::$app->request->post()) && $Record->save()){
            Yii::$app->session->setFlash('success','Record updated');
            return $this->redirect(['index']);
        }
        return $this->render('update',['model'=>$Record]);
    }

}
