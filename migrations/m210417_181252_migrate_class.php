<?php

use yii\db\Migration;

/**
 * Class m210417_181252_migrate_class
 */
class m210417_181252_migrate_class extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('records', [
           'id'=> $this->primaryKey(),
            'title' => $this->string(64)->notNull(),
            'description' => $this->string(64),
        ]);
        $this->createTable('categories',[
           'id'=>$this->primaryKey(),
           'record_id'=>$this->integer(),
            'cat_description'=>$this->string(255)->notNull()
           ]);
        $this->createIndex(
            'idx-records-rec_id',
            'records',
            'id'
        );
        $this->addForeignKey('fk-records-rec_id',
            'categories',
            'record_id',
            'records',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210417_181252_migrate_class cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210417_181252_migrate_class cannot be reverted.\n";

        return false;
    }
    */
}
