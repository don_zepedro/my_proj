<?php
/* @var $this yii\web\View */
use yii\helpers\Url;
?>
<h1>List of all Records
<a href="<?= Url::to(['create'])?>" class="btn btn-primary">Create new record</a></h1>
<br><br>
<p>
    <table class="table table-condensed">
    <tr>
        <th>Id</th>
        <th>Title</th>
        <th>Description</th>
        <th>Edit</th>
        <th>Delete</th>
    </tr>
        <?php foreach ($Records as $k=>$v) :?>
    <tr>

        <th><?= $v['id'] ?></th>
        <th><?= $v['title'] ?></th>
        <th><?= $v['description'] ?></th>

        <th><a href="<?= Url::to(['update?id=' . $v['id']])?>" class="btn btn-primary">Edit</a></th>
        <th><a href="<?= Url::to(['delete?id=' . $v['id']])?>" class="btn btn-primary">Delete</a></th>
    </tr>

        <?php endforeach;?>

    </table>
</p>
