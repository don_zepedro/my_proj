<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

    <h1>Update Record</h1>

<?php $form = ActiveForm::begin(); ?>
<?php echo $form->field($model,'title')?>
<?php echo $form->field($model,'description')?>
<?php echo Html::submitButton('Save',[
    'class' => 'btn btn-primary'
]);?>

<?php ActiveForm::end(); ?>