<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "categories".
 *
 * @property int $id
 * @property int|null $record_id
 * @property string $cat_description
 *
 * @property Records $record
 */
class Categories extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['record_id'], 'integer'],
            [['record_id'], 'required'],
            [['cat_description'], 'required'],
            [['cat_description'], 'string', 'max' => 255],
            [['record_id'], 'exist', 'skipOnError' => true, 'targetClass' => Records::class, 'targetAttribute' => ['record_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'record_id' => 'Record ID',
            'cat_description' => 'Cat Description',
        ];
    }

    /**
     * Gets query for [[Record]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecord()
    {
        return $this->hasOne(Records::class, ['id' => 'record_id']);
    }

}
